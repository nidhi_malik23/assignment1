﻿using System;
using System.Collections.Generic;
using System.Text;

namespace assignmenttry.Domain
{
    public class Actor
    {
       
        public Actor(string name, string dob)
        {
            this.name = name;
            this.dob = dob;
            id++;
            actorId = id;
        }
        private static int id = 0;
        public int actorId;
        public string name { get; set; }
        public string dob { get; set; }
        public override string ToString()
        {
            return "Actor Id: " + actorId + "\t Actor Name: " + name + "\t Actor DOB: " + dob;
        }
    }
}
