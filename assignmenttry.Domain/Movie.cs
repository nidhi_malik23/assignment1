﻿using System;
using System.Collections.Generic;
using System.Text;

namespace assignmenttry.Domain
{
    public class Movie
    {
        int year;
        public string name { get; set; }
        List<Actor> actors = new List<Actor>();
        Producer producer;
        string plot;
        public int movieId { get; set; }

        public Movie(int year, string name, List<Actor> actors, Producer producer, string plot)
        {
            this.year = year;
            this.name = name;
            this.actors = actors;
            this.producer = producer;
            this.plot = plot;
            idIncrementer++;
            this.movieId = idIncrementer;
            
        }

        private static int idIncrementer = 0;
        public override string ToString() {
            String data =  "Movie Id:"+movieId+ " \t Movie Name: " + name + "\t Movie Release Date: " + year + " Movie plot: " + plot+"\n";
            data += "Actors:\n";
            foreach(var actor in actors)
            {
                data += actor.ToString();
            }
            data += "\n Producer: " + producer.ToString();
            return data;
        }

    }
}
