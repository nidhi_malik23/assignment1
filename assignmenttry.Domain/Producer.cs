﻿using System;
using System.Collections.Generic;
using System.Text;

namespace assignmenttry.Domain
{
    public class Producer
    {
 
        public Producer(string name, string dob)
        {
            this.name = name;
            this.dob = dob;
            id++;
            this.producerId = id;
        }
        private static int id = 0;
        public int producerId;
        public string name { get; set; }
        public string dob { get; set; }
        public override string ToString()
        {
            return "Producer Id: " + producerId + "\t Producer Name: " + name + "\t Producer DOB: " + dob;
        }
    }
}
