﻿using System;
using TechTalk.SpecFlow;

using System.Collections.Generic;
using assignmenttry;
using assignmenttry.Domain;
using assignmenttry.Repository;

using TechTalk.SpecFlow.Assist;
using Xunit;


namespace assignmenttry.Tests
{
    [Binding]
    public class AssignmenttrySteps
    {
        
        string name;
        string plot;
        int producerId, release;
        List<int> actorIds = new List<int>();
        List<Movie> movieList = new List<Movie>();
        List<Actor> _actorsList = new List<Actor>();
        List<Producer> _producersList = new List<Producer>();

        MovieRepository movieRepository = new MovieRepository();
        ActorRepository actorRepository = new ActorRepository();
        ProducerRepository producerRepository = new ProducerRepository();


        [Given(@"I have a movie with name ""(.*)""")]
        public void GivenIHaveAMovieWithName(string p0)
        {
            name = p0;
        }
        
        [Given(@"actor ids are ""(.*)""")]
        public void GivenActorIdsAre(string p0)
        {
            var ids = p0.Split(" ");
                foreach(var id in ids)
            {
                this.actorIds.Add(Convert.ToInt32(id));
            }
        }
        
        [Given(@"producer id  is ""(.*)""")]
        public void GivenProducerIdIs(string p0)
        {
            producerId = Convert.ToInt32(p0);
        }
        
        [Given(@"release year is ""(.*)""")]
        public void GivenReleaseYearIs(string p0)
        {
            this.release = Convert.ToInt32(p0);
        }
        [Given(@"plot is ""(.*)""")]
        public void GivenPlotIs(string plot)
        {
            this.plot = plot;
        }


        [Given(@"I have a app of movies")]
        public void GivenIHaveAAppOfMovies()

        {
           
        }

        void populateActorsAndProducers()
        {
            this.producerRepository.addProducer("zxc", "23/11/1998");
            this.actorRepository.addActor("abc", "26/11/1999");
            this.actorRepository.addActor("hjs", "24/12/1989");
            this._actorsList = this.actorRepository.getAllActors();
            this._producersList = this.producerRepository.getAllProducers();
    
        }


        [When(@"I add the movie in  the app")]
        public void WhenIAddTheMovieInTheApp()

        {
            populateActorsAndProducers();
            List<Actor> actors = new List<Actor>();
            foreach(var actorId in this.actorIds){
                actors.Add(actorRepository.getActorById(actorId));
            }
            Producer producer = this.producerRepository.getProducerById(this.producerId);

            this.movieRepository.addMovie(this.release,this.name, this.plot,actors,producer);
        }

        [When(@"I fetch all movies")]
        public void WhenIFetchAllMovies()
        {
            movieRepository.getAllMovies();
        }
        
        [Then(@"my movie list look like")]
        public void ThenMyMovieListLookLike(Table table)
        {
            List<Movie> _movielist = movieRepository.getAllMovies();
            table.FindInSet(_movielist);
            //table.CompareToSet(_movielist);
        }
        
        [Then(@"the actors of movies look like")]
        public void ThenTheActorsOfMoviesLookLike(Table table)
        {
            table.FindInSet(_actorsList);
        }
        
        [Then(@"the producers of movies look like")]
        public void ThenTheProducersOfMoviesLookLike(Table table)
        {
            table.FindInSet(_producersList);
        }
        
        [Then(@"I should have the following movie")]
        public void ThenIShouldHaveTheFollowingMovie(Table table)
        {
            List<Movie> _movielist = movieRepository.getAllMovies();
            table.FindInSet(_movielist);
        }
    }
}
