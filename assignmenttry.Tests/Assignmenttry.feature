﻿Feature: Assignmenttry
	
@emptyMovieList
Scenario: add a movie to the app
	Given I have a movie with name "Bhaubali"
	And actor ids are "1 2"
	And producer id  is "1"
	And release year is "2015"
	And  plot is "dggdh"
	When I add the movie in  the app
	Then my movie list look like
	| name       | year | plot  |
	| Bhaubali  | 2015    | dggdh |
	And the actors of movies look like
	| actorId | name | dob        |
	| 1       | abc       | 26/11/1999 |
	| 2       | hjs       | 24/12/1989 |
	And the producers of movies look like
	| producerId | name | dob        |
	| 1          | zxc          | 23/11/1998 |
@listMovie
Scenario: List all movies in myapp
	Given I have a app of movies
	When I fetch all movies
	Then I should have the following movie
	| name       | year | plot  |
	| Bhaubali  | 2015    | dggdh |
	And the actors of movies look like
	| actorId | name | dob        |
	| 1       | abc       | 26/11/1999 |
	| 2       | hjs       | 24/12/1989 |
	And the producers of movies look like
	| producerId | name | dob        |
	| 1          | zxc          | 23/11/1998 |


