﻿using System;
using System.Collections.Generic;
using System.Text;
using assignmenttry.Domain;

namespace assignmenttry.Repository
{
    public class MovieRepository
    {
        List<Movie> moviesListCache = new List<Movie>();
        public List<Movie> getAllMovies()
        {
            return moviesListCache;
        }
        public void addMovie(int year, string name, string plot, List<Actor> actor, Producer producer)
        {
            this.moviesListCache.Add(new Movie(year, name, actor, producer, plot));
        }
        public void deleteMovieById(int id)
        {
            var movie = this.moviesListCache.Find(movie => movie.movieId == id);
            if (movie != null)
            {
                this.moviesListCache.Remove(movie);
            }

        }
        public void deleteMovieByName(string name)
        {
            var movie = this.moviesListCache.Find(movie => movie.name == name);
            this.moviesListCache.Remove(movie);
        }
    }
}
