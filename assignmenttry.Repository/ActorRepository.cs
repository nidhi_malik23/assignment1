﻿using System;
using System.Collections.Generic;
using System.Text;
using assignmenttry.Domain;

namespace assignmenttry.Repository
{
    public class ActorRepository
    {
        List<Actor> actorsListCache = new List<Actor>();
        public List<Actor> getAllActors()
        {
            return actorsListCache;
        }
        public void addActor(string name, string dob)
        {
            this.actorsListCache.Add(new Actor(name, dob));
        }
        public Actor getActorById(int id)
        {
            return this.actorsListCache.Find(actor => actor.actorId == id);
        }
    }
}
