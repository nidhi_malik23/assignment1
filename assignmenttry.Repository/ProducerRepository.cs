﻿using System;
using System.Collections.Generic;
using System.Text;
using assignmenttry.Domain;

namespace assignmenttry.Repository
{
    public class ProducerRepository
    {
        List<Producer> producersListCache = new List<Producer>();

        public List<Producer> getAllProducers()
        {
            return producersListCache;
        }
    
    public void addProducer(string name, string dob)
    {
        this.producersListCache.Add(new Producer(name, dob));
    }

    public Producer getProducerById(int id)
    {
        return this.producersListCache.Find(producer => producer.producerId == id);
    }
}
}
