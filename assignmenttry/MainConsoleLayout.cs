﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using assignmenttry.Repository;
using assignmenttry.Domain;

namespace assignmenttry
{
    public class MainConsoleLayout
    {
        SortedList<int, string> menuOptions = new SortedList<int, string>();
        ActorRepository actorRepository = new ActorRepository();
        MovieRepository movieRepository = new MovieRepository();
        ProducerRepository producerRepository = new ProducerRepository();

        void loadOptions()
        {
            menuOptions.Add(1, "List Movie");
            menuOptions.Add(2, "Add Movie");
            menuOptions.Add(3, "Add Actor");
            menuOptions.Add(4, "Add Producer");
            menuOptions.Add(5, "Delete Movie");
            menuOptions.Add(6, "Exit");
        }

        public void start()
        {
            loadOptions();
            Console.WriteLine("Welcome to my Kingdom");
            foreach (var menuOpt in menuOptions)
            {
                Console.WriteLine(menuOpt.Key + ". " + menuOpt.Value);
            }
            Console.WriteLine("Choose One Option");
            var inputId = Convert.ToInt32(Console.ReadLine());
            while (inputId != 6)
            {
                invokeOperationById(inputId);
                Console.WriteLine("Choose One Option");
                inputId = Convert.ToInt32(Console.ReadLine());
            }

        }

        void invokeOperationById(int inputId)
        {
            switch (inputId)
            {
                case 1:
                    printObjectsList<Movie>(movieRepository.getAllMovies());
                    break;
                case 2:
                    addMovieFromConsoleInput();
                    break;
                case 3:
                    addActorFromConsoleInput();
                    break;
                case 4:
                    addProducerFromConsoleInput();
                    break;
                case 5:
                    deleteMovieFromConsoleInput();
                    break;
                case 6:
                    Environment.Exit(0);
                    break;

                default:
                    Console.WriteLine("Invalid Input");
                    break;
            }
        }


        public void addMovieFromConsoleInput()
        {
            Console.WriteLine("Enter the name of movie to be added");
            var name = Console.ReadLine();
            Console.WriteLine("Enter the relase year of movie to be added");
            var release = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter the plot of movie to be added");
            var plot = Console.ReadLine();
            Console.WriteLine("Enter the id's among listed acotrs to be added for the movie");
            printObjectsList<Actor>(actorRepository.getAllActors());
            var chosenActors = Console.ReadLine();
            var actorsIds = chosenActors.Split(" ");

            // "1 2 3 4 5"
            // ["1","2","3"]
            // [1,2,3,4,5]
            List<Actor> actorsList = new List<Actor>();
            foreach (var id in actorsIds)
            {
                var parsedId = Convert.ToInt32(id);
                var actor = actorRepository.getActorById(parsedId);
                if (actor != null)
                {
                    actorsList.Add(actor);
                }
                else
                {
                    Console.WriteLine("Actor with Id " + parsedId + " not Found");
                }
            }
            Console.WriteLine("Enter the id's among listed producers to be added for the movie");
            printObjectsList<Producer>(producerRepository.getAllProducers());
            var producerId = Console.ReadLine();
            var paseProducerId = Convert.ToInt32(producerId);
            var producer = producerRepository.getProducerById(paseProducerId);
            if (producer == null)
            {
                Console.WriteLine("Producer with Id " + paseProducerId + " not Found");
            }
            else
            {
                movieRepository.addMovie(release, name, plot, actorsList, producer);
            }

        }
        public void addActorFromConsoleInput()
        {
            Console.WriteLine("enter the name of actor");
            var actorName = Console.ReadLine();
            Console.WriteLine("enter dob of actor");
            var actorDob = Console.ReadLine();
            actorRepository.addActor(actorName, actorDob);
        }
        public void addProducerFromConsoleInput()
        {
            Console.WriteLine("enter the name of producer");
            var producerName = Console.ReadLine();
            Console.WriteLine("enter dob of producer");
            var producerDob = Console.ReadLine();
            producerRepository.addProducer(producerName, producerDob);
        }
        public void deleteMovieFromConsoleInput()
        {
            Console.WriteLine("print the list of movies");
            printObjectsList<Movie>(movieRepository.getAllMovies());
            Console.WriteLine("enter id of movie");
            var movieId = Convert.ToInt32(Console.ReadLine());
            movieRepository.deleteMovieById(movieId);
        }

        public void printObjectsList<T>(List<T> objects)
        {
            foreach (var obj in objects)
            {
                Console.WriteLine(obj.ToString());
            }
        }
    }
    
}
